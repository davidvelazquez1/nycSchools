//
//  CurrentSchoolStore.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Combine
import Foundation

protocol CurrentSchoolStoreType {
    func publish(_ value: ValidHighSchool)
    var publisher: AnyPublisher<ValidHighSchool?, Never> { get }
}

class CurrentSchoolStore: CurrentSchoolStoreType {
    private init(){}
    static let shared: CurrentSchoolStore = CurrentSchoolStore()
    
    private let _publisher: CurrentValueSubject<ValidHighSchool?, Never> = .init(nil)
    
    func publish(_ value: ValidHighSchool) {
        _publisher.send(value)
    }
    
    var publisher: AnyPublisher<ValidHighSchool?, Never> {
        _publisher.eraseToAnyPublisher()
    }
}
