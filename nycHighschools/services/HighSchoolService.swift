//
//  HighSchoolService.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Combine
import Foundation

protocol HighSchoolServiceType {
    func fetchAllSchools() -> AnyPublisher<[HighSchool], Error>
}

class HighSchoolService: HighSchoolServiceType {
    
    private let session: URLSession
    private let url: String
    
    init(
        session: URLSession = URLSession.shared,
        url: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    ) {
        self.session = session
        self.url = url
    }
    
    func fetchAllSchools() -> AnyPublisher<[HighSchool], Error> {
        guard let url = URL(string: url) else {
            return Fail(error: Errors.malformedUrl).eraseToAnyPublisher()
        }
        // TODO: Add http code validation
        return session.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: [HighSchool].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}

extension HighSchoolService {
    enum Errors: Error {
        case malformedUrl
    }
}
