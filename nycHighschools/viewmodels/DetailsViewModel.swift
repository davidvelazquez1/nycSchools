//
//  DetailsViewModel.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Combine
import Foundation

class DetailsViewModel: ObservableObject {
    
    private let store: CurrentSchoolStoreType
    private let navigator: Navigator
    
    init(store: CurrentSchoolStoreType = CurrentSchoolStore.shared, navigator: Navigator = NavigationHolder.shared) {
        self.store = store
        self.navigator = navigator
    }
    
    private var cancellables: Set<AnyCancellable> = []
    
    @Published var title: String = ""
    @Published var description: String = ""
    @Published var dbn: String = ""
    
    func initialize() {
        store.publisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] school in
                if let school = school {
                    self?.publish(school)
                } else {
                    self?.description = "Something went wrong"
                    
                    // TODO: use constants to avoid magic number 5
                    let deadline: DispatchTime = .now() + 5
                    DispatchQueue.main.asyncAfter(deadline: deadline) { [weak self] in
                        self?.navigator.popBack()
                    }
                }
            }
            .store(in: &cancellables)
    }
    
    private func publish(_ school: ValidHighSchool) {
        // TODO: use Localizable strings
        title = school.name
        dbn = "School dbn: \(school.id)"
        description = school.paragraph
    }
}
