//
//  ListViewModel.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Combine
import Foundation

class ListViewModel: ObservableObject {
    
    private let service: HighSchoolServiceType
    private let selectedSchoolStore: CurrentSchoolStoreType
    private let navigator: Navigator
    
    init(
        service: HighSchoolServiceType = HighSchoolService(),
        store: CurrentSchoolStoreType = CurrentSchoolStore.shared,
        navigator: Navigator = NavigationHolder.shared
    ) {
        self.service = service
        self.selectedSchoolStore = store
        self.navigator = navigator
    }
    
    @Published var content: ViewContent = .loading
    private var schools: [HighSchool] = []
    private var cancellables: Set<AnyCancellable> = []
    
    func initialize() {
        service.fetchAllSchools()
            .map { $0.toViewRepresentable() }
            .flatMap { [weak self] value in
                self?.validateThereAreSchoolsAvailable(value) ?? Fail(error: Errors.selfNotFound).eraseToAnyPublisher()
            }
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished: break // No op
                case .failure(let error):
                    self?.publishFailure(error)
                }
            } receiveValue: { [weak self] schools in
                self?.content = .success(schools)
            }
            .store(in: &cancellables)
    }
    
    private func publishFailure(_ error: Error) {
        // TODO: remove prints
        debugPrint(error)
        content = .error
    }
    
    private func validateThereAreSchoolsAvailable(_ schools: [ValidHighSchool]) -> AnyPublisher<[ValidHighSchool], Error> {
        if schools.isEmpty {
            return Fail(error: Errors.noSchoolsFound).eraseToAnyPublisher()
        } else {
            return Just(schools).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
    }
    
    func onSchoolSelected(_ school: ValidHighSchool) {
        selectedSchoolStore.publish(school)
        navigator.navigateToDetails()
    }
}

private extension [HighSchool] {
    func toViewRepresentable() -> [ValidHighSchool] {
        self.compactMap { school in
            guard let name = school.name, let paragraph = school.overviewParagraph else { return nil }
            return ValidHighSchool(id: school.id, name: name, paragraph: paragraph)
        }
    }
}

extension ListViewModel {
    enum Errors: Error {
        case noSchoolsFound
        case selfNotFound
    }
    
    enum ViewContent {
        case error
        case loading
        case success([ValidHighSchool]) // TODO: change for view representable
    }
}
