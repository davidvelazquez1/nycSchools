//
//  nycHighschoolsApp.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import SwiftUI

@main
struct nycHighschoolsApp: App {
    var body: some Scene {
        WindowGroup {
            Coordinator()
        }
    }
}
