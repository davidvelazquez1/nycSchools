//
//  Coordinator.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import SwiftUI

struct Coordinator: View {
    @StateObject var navigator = NavigationHolder.shared
    
    var body: some View {
        NavigationStack(path: $navigator.route) {
            HighSchoolListView()
                .navigationDestination(for: NavigationHolder.RouteSection.self) { route in
                    switch route {
                    case .details: SchoolDetailsView()
                    }
                }
        }
    }
}
