//
//  Navigator.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Foundation

protocol Navigator {
    func navigateToDetails()
    func popBack()
}

class NavigationHolder: Navigator, ObservableObject {
    private init(){}
    static let shared = NavigationHolder()
    
    @Published var route: [RouteSection] = []
    
    func navigateToDetails() {
        route.append(.details)
    }
    
    func popBack() {
        route = route.dropLast()
    }
    
    enum RouteSection {
        case details
    }
}
