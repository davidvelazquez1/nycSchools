//
//  HighSchoolCellRepresentable.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Foundation

struct ValidHighSchool {
    let id: String
    let name: String
    let paragraph: String
}

extension ValidHighSchool: Identifiable {}
