//
//  HighSchool.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Foundation

struct HighSchool {
    let id: String
    let name: String?
    let overviewParagraph: String?
    let neighborhood: String?
}

extension HighSchool: Codable {
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case overviewParagraph = "overview_paragraph"
        case neighborhood
    }
}
