//
//  SchoolDetailsView.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import SwiftUI

struct SchoolDetailsView: View {
    
    @StateObject var viewModel: DetailsViewModel = DetailsViewModel()
    
    var body: some View {
        VStack {
            Text(viewModel.title)
                .font(.title)
                .padding(.bottom)
            Text(viewModel.dbn)
                .font(.title3)
            Spacer()
            Text(viewModel.description)
                .font(.body)
            Spacer()
        }
        .padding()
        .onAppear { viewModel.initialize() }
    }
}
