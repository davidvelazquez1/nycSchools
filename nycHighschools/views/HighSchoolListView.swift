//
//  HighSchoolListView.swift
//  nycHighschools
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import SwiftUI

struct HighSchoolListView: View {
    
    @StateObject var viewModel = ListViewModel()
    
    var body: some View {
        buildView()
            .onAppear { viewModel.initialize() }
    }
    
    @ViewBuilder
    func buildView() -> some View {
        switch viewModel.content {
        case .error: Button(
            "Ooops, something went wrong.\n click here to retry",
            action: viewModel.initialize
        )
        case .loading: ProgressView()
        case .success(let schools): List(schools) { school in
            Text(school.name)
                .onTapGesture {
                    viewModel.onSchoolSelected(school)
                }
                .accessibilityAddTraits(.isButton)
                .accessibilityHint("Click to see school details")
        }
        }
    }
}

#Preview {
    HighSchoolListView()
}
