//
//  TestErrors.swift
//  nycHighschoolsTests
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

import Foundation

enum TestErrors: Error {
    case defaultError
}
