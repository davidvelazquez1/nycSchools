//
//  CurrentSchoolStoreTests.swift
//  nycHighschoolsTests
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

@testable import nycHighschools
import XCTest

final class CurrentSchoolStoreTests: XCTestCase {
    
    private var store: CurrentSchoolStore!
    
    override func setUp() {
        self.store = CurrentSchoolStore.shared
    }
    
    func test_publishing_posts_school() {
        // Given
        let validSchool = ValidHighSchool(id: "2", name: "mcLure", paragraph: "doing great things for our kids")
        
        // When
        store.publish(validSchool)
        
        // Then
        let waiter = XCTestExpectation(description: "school should be published")
        _ = store.publisher.sink { school in
            XCTAssert(school != nil)
            XCTAssertEqual(school?.id, validSchool.id)
            waiter.fulfill()
        }
        wait(for: [waiter], timeout: 3)
    }

}
