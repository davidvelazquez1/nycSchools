//
//  ListViewModelTests.swift
//  nycHighschoolsTests
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

@testable import nycHighschools
import Combine
import XCTest

final class ListViewModelTests: XCTestCase {

    private var navigator: MockNavigator!
    private var store: MockSchoolStore!
    private var service: MockHighSchoolService!
    private var viewModel: ListViewModel!
    
    private var cancellables: Set<AnyCancellable> = []
    
    override func setUp() {
        navigator = MockNavigator()
        store = MockSchoolStore()
        service = MockHighSchoolService()
        viewModel = ListViewModel(service: service, store: store, navigator: navigator)
    }
    
    func test_loadingOfValidSchools() {
        // Given
        let schools = [HighSchool(id: "1", name: "McLure", overviewParagraph: "doing great things", neighborhood: "Alphareta GA")]
        service.schools = schools
        
        // When
        viewModel.initialize()
        
        // Then
        let expectation = XCTestExpectation(description: "schools should be published")
        viewModel.$content.sink { value in
            switch value {
            case .error: XCTFail()
            case .loading: break
            case .success(_): expectation.fulfill()
            }
        }.store(in: &cancellables)
        
        wait(for: [expectation], timeout: 5)
    }
    
    func test_loading_with_no_ValidSchools() {
        // Given
        let schools = [HighSchool(id: "1", name: nil, overviewParagraph: "doing great things", neighborhood: "Alphareta GA")]
        service.schools = schools
        
        // When
        viewModel.initialize()
        
        // Then
        let expectation = XCTestExpectation(description: "error should be published")
        viewModel.$content.sink { value in
            switch value {
            case .error: expectation.fulfill()
            case .loading: break
            case .success(_): XCTFail()
            }
        }.store(in: &cancellables)
        
        wait(for: [expectation], timeout: 5)
    }
}
