//
//  MockSchoolStore.swift
//  nycHighschoolsTests
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

@testable import nycHighschools
import Combine
import Foundation

class MockSchoolStore: CurrentSchoolStoreType {
    
    var hasPublishBeenCalled = false
    func publish(_ value: nycHighschools.ValidHighSchool) {
        hasPublishBeenCalled = true
    }
    
    var mockPublisher = CurrentValueSubject<ValidHighSchool?, Never>(nil)
    var publisher: AnyPublisher<ValidHighSchool?, Never> {
        mockPublisher.eraseToAnyPublisher()
    }
}
