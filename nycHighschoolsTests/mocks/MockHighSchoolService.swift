//
//  MockHighSchoolService.swift
//  nycHighschoolsTests
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

@testable import nycHighschools
import Combine
import Foundation

class MockHighSchoolService: HighSchoolServiceType {
    
    var schools = [HighSchool]()
    func fetchAllSchools() -> AnyPublisher<[HighSchool], Error> {
        if schools.isEmpty {
            return Fail(error: TestErrors.defaultError).eraseToAnyPublisher()
        } else {
            return Just(schools).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
    }
}
