//
//  MockNavigator.swift
//  nycHighschoolsTests
//
//  Created by Dionicio Cruz Velázquez on 2/15/24.
//

@testable import nycHighschools
import Foundation

class MockNavigator: Navigator {
    
    var hasNavigateToDetailsBeenCalled = false
    func navigateToDetails() {
        hasNavigateToDetailsBeenCalled = true
    }
    
    var hasPopBackBeenCalled = false
    func popBack() {
        hasPopBackBeenCalled = true
    }
}
